import { SET_SESSION } from "./action.types";

export const setSessionAction = session => ({
    type: SET_SESSION,
    session
});