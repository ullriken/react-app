import { combineReducers } from "redux";
import surveyReducer from './survey.reducer';
import sessionReducer from './session.reducer'

export default combineReducers({
    surveys: surveyReducer,
    session: sessionReducer
});