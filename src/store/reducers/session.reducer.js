import { SET_SESSION } from "../actions/action.types";

const initialState = {
    token: '',
    user: {}
}

const sessionReducer = (state = initialState, action = {})=> {
    switch (action.type) {
        case SET_SESSION:
            return action.session;
        default:
            return state
    }
}

export default sessionReducer;