// State = Surveys
import { SET_SURVEYS, ADD_SURVEY, DELETE_SURVEY, UPDATE_SURVE } from "../actions/action.types";

const surveyReducer = (state = [], action = {}) => {
    //checks
    switch ( action.type ) {
        case SET_SURVEYS:
            return [...action.surveys];
        case ADD_SURVEY:
            return [
                ...state,
                action.survey
            ]
        case DELETE_SURVEY: {
            const filteredSurveys = state.filter(survey => survey.survey_Id !== action.survey_Id)
            return [
                ...filteredSurveys
            ]
        }
        case UPDATE_SURVE: {
            return [
                ...state
            ]
        }

        default:
            return state;;
    }
}

export default surveyReducer;