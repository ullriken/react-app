export const setStorage = (key, value) => {
    const json = JSON.stringify( value );
    const ecrypted = btoa( json );
    localStorage.setItem( key, ecrypted );
}

export const getStorage = ( key ) => {
    const storedValue = localStorage.getItem( key );
    if(!storedValue) return false;
    return JSON.parse( atob( storedValue ) );
}