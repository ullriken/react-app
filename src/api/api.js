import axios from 'axios';

export const BASE_URL = 'https://survey-poodle.herokuapp.com/v1/api';

export const handleResponse = response => {
    if(response.status >= 400){
        throw Error(response.error);
    }
    return response;
}

export const setAuthHeader = (store) =>  {
    store.subscribe(()=>{

        const {session: {token} } = store.getState();

        if ( token ) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
        } else {
            delete axios.defaults.headers.common['Authorization']
        }
    });
}