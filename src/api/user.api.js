import { handleResponse } from "./api";
import { BASE_URL } from './api'

const createFetchOptions = (method, body) => ({
    method: method,
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
});

export const loginUser = (username, password) => {
    return fetch(
        BASE_URL +'/users/login',
        createFetchOptions('POST', {
            user: {
                username,
                password
            }
        })
    ).then(r => r.json())
    .then(handleResponse);
}

export const registerUser = (username, password) => {
    return fetch(
        BASE_URL+'/users/register',
        createFetchOptions('POST', {
            user: {
                username,
                password
            }
        })
    ).then(r => r.json())
    .then(handleResponse);
}