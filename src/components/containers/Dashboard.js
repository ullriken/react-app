import React from 'react';
import SurveyList from '../lists/SurveyList';
import { getStorage } from '../../utils/localStorage';
import { Redirect } from 'react-router-dom';

const Dashboard = ()=> {

    const { token } = getStorage('ra_session');

    return (
        <div>
            { !token ? <Redirect to="/login"/> :
            <div>
                <h1>Welcome to the Dashboard</h1>
            <aside>
                Profile go here
            </aside>
            <main>
                <SurveyList/>
            </main>
            </div>
            }
            
        </div>
    )
};

export default Dashboard;