import React from 'react';
import RegisterForm from '../forms/RegisterForm';
import { Link, useHistory, Redirect } from 'react-router-dom';
import { getStorage, setStorage } from '../../utils/localStorage';

const Register = () => {

    const { token } = getStorage('ra_session');
    const history = useHistory();

    const handleRegisterComplete = ({ data }) => {
        if (data && data.token){
            const {token, user} = data;
            if (token){
                setStorage('ra_session', {
                    user,
                    token
                });
                history.replace('/dashboard');
            }
        }
    }

    return (
        <div>
            { token && <Redirect to="/dashboard" /> }
            <h1>Register for Survey Puppy</h1>
            <RegisterForm complete={ handleRegisterComplete } />
            <Link to="/login" > Already have an account? Login here</Link>
        </div>
    );
}

export default Register;