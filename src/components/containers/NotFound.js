import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
    <div>
        <img src="https://image.freepik.com/free-vector/error-404-concept-landing-page_23-2148235657.jpg" alt="notFound"></img>
        <Link to="/login">Go back Home</Link>
    </div>
);

export default NotFound;