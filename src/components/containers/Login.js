import React from 'react';
import LoginForm from '../forms/LoginForm';
import { Link, Redirect, useHistory } from 'react-router-dom';
import { setStorage, getStorage } from '../../utils/localStorage';
import { useDispatch } from 'react-redux';
import { setSessionAction } from '../../store/actions/session.action'

const Login = ()=> {
    
    const { token } = getStorage('ra_session');
    const history = useHistory();
    const dispatch = useDispatch();

    const onLoginComplete = ({user, token}) => {
        const session = { user,token }
        setStorage('ra_session',session);
        dispatch(setSessionAction( session ));
        history.replace('/dashboard');
    }

    return (
        <div>
            { token && <Redirect to="/dashboard"/>}
            <h1> Login to Survey</h1>
            <LoginForm complete={onLoginComplete} />
            <Link to="/register" > No account? Register here</Link>
        </div>
    );
}
export default Login;