import React, { useState } from 'react';
import { loginUser } from '../../api/user.api';

const LoginForm = (props)=> {

    const [ username, setUsername ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ isLoading, setIsLoading ] = useState(false);
    const [ loginError, setLoginError ] = useState('');

    const onLoginClicked = async ev => {

        setIsLoading(true);
        setLoginError('');

        let loginResult;

        try {
            loginResult = await loginUser( username, password );
        } catch (e) {
            setLoginError( e.message || e );
        } finally {
            setIsLoading(false);
            if (loginResult && loginResult.hasOwnProperty('data')) {
                props.complete(loginResult.data);
            }
        }
    }

    const onUsernameChanged = ev => setUsername(ev.target.value.trim());
    const onPasswordChanged = ev => setPassword(ev.target.value.trim());


    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter your Username" onChange={ onUsernameChanged } />
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter your Password" onChange={ onPasswordChanged } />
            </div>
            <div>
                <button type="button" onClick={ onLoginClicked }>Login</button>
            </div>

            { isLoading && <div>Loggging in user...</div> }
            { loginError && <div>{loginError}</div> }
            
        </form>
    )
};

export default LoginForm;