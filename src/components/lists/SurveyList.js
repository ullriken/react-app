import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getSurveys } from '../../api/surveys.api';
import { setSurveyActions } from '../../store/actions/survey.actions'

const SurveyList = ()=>{

    const dispatch = useDispatch();
    const surveys = useSelector(state => state.surveys);
    
    useEffect(()=>{
        getSurveys().then( ({data, status, error}) => {
            dispatch( setSurveyActions( data ) )
        }).catch(error => {
            console.log(error);
        })
    }, [ dispatch ]);

    const surveyList = surveys.map(survey => (
        <li key={survey.survey_id}>
            <h4>{ survey.title }</h4>
            <p>{ survey.description }</p>
            <small>{ survey.user.username }</small>
        </li>
    ))

    return (
        <div>
            <ul>
                {surveyList}
            </ul>
        </div>
    )
}

export default SurveyList;